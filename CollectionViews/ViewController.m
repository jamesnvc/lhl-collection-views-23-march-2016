//
//  ViewController.m
//  CollectionViews
//
//  Created by James Cash on 23-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CircleLayout.h"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) UICollectionViewFlowLayout *bigLayout;
@property (strong,nonatomic) UICollectionViewFlowLayout *smallLayout;
@property (strong,nonatomic) CircleLayout *circleLayout;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.bigLayout = [[UICollectionViewFlowLayout alloc] init];
//    self.bigLayout.itemSize = CGSizeMake(150, 150);
//    self.bigLayout.estimatedItemSize = CGSizeMake(130, 130);
    self.bigLayout.minimumInteritemSpacing = 75;
    self.bigLayout.minimumLineSpacing = 100;
    self.bigLayout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20);
    self.bigLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 100);
    self.bigLayout.footerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 100);

    self.smallLayout = [[UICollectionViewFlowLayout alloc] init];
//    self.smallLayout.itemSize = CGSizeMake(75, 75);
//    self.smallLayout.estimatedItemSize = CGSizeMake(100, 100);
    self.smallLayout.minimumInteritemSpacing = 25;
    self.smallLayout.minimumLineSpacing = 10;
    self.smallLayout.sectionInset = UIEdgeInsetsMake(10, 20, 10, 20);
    self.smallLayout.headerReferenceSize = CGSizeMake(300, 50);
    self.smallLayout.footerReferenceSize = CGSizeMake(300, 50);

    self.circleLayout = [[CircleLayout alloc] init];

    self.collectionView.collectionViewLayout = self.bigLayout;
    self.collectionView.delegate = self;
//    [self.collectionView setCollectionViewLayout:self.bigLayout]
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleLayout:(id)sender {
    UICollectionViewLayout *old = self.collectionView.collectionViewLayout;
    UICollectionViewLayout *next = nil;
    if (old == self.bigLayout) {
        next = self.smallLayout;
    } else if (old == self.smallLayout) {
        next = self.circleLayout;
    } else {
        next = self.bigLayout;
    }
//    self.collectionView.collectionViewLayout = next;
    [self.collectionView setCollectionViewLayout:next animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionViewLayout == self.bigLayout) {
        return CGSizeMake(100 + 10*indexPath.item, 100 + 10*indexPath.item);
    } else if (collectionViewLayout == self.smallLayout) {
        return CGSizeMake(100 - 10*indexPath.item, 100 - 10*indexPath.item);
    }
    return CGSizeZero;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3 + section;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"demoCell" forIndexPath:indexPath];

    UILabel *label = [cell viewWithTag:42];
    label.text = [NSString stringWithFormat:@"%ld, %ld", (long)indexPath.section, (long)indexPath.item];

    switch (indexPath.section) {
        case 0:
            cell.backgroundColor = [UIColor blueColor];
            break;
        case 1:
            cell.backgroundColor = [UIColor redColor];
            break;
        default:
            cell.backgroundColor = [UIColor greenColor];
            break;
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"demoHeader" forIndexPath:indexPath];
        return header;
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"demoFooter" forIndexPath:indexPath];
        return footer;
    }
    return nil;
}

@end
